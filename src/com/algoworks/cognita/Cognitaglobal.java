package com.algoworks.cognita;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.algoworks.dataprovider.CognitaDataProvider;
import com.algoworks.pagefactory.Cognita;
import com.algoworks.pagefactory.Login;
import com.algoworks.utility.Browser;
import com.algoworks.utility.CustomWait;
import com.algoworks.utility.SpecialActions;
import com.algoworks.utility.WriteExcel;
import com.google.common.base.Throwables;

public class Cognitaglobal
{
	private int ngraphrow = -1;
	private WebDriver driver;
	private WriteExcel write = new WriteExcel();
	private Cognita cognita;
	private boolean doesExcelExist = false;
	private String excelFilename;
	private CustomWait wait = null;
	private String[] yoeData =
	{ "All", "CYOE", "NYOE", "NNYOE", "2016/17", "2017/18", "2018/19" };
	private By yearsAxis = By.cssSelector("span#enq_chart_id.fusioncharts-container svg g.fusioncharts-legend");
	private By inMonthbtn = By.cssSelector("button.ui-button.in_month_btn");
	private By accumulatedBtn = By.xpath("//div[@id='lpc__AdmissionsAndMarketing_WAR_cognita_globalportlet_INSTANCE_0mbjA0FvKk3A_AnalyticContainer']/div[2]/div[2]/div[2]/h3[3]/div[2]/button");

	@BeforeClass
	public void setup(final ITestContext context) throws InterruptedException
	{
		excelFilename = context.getCurrentXmlTest().getParameter("outputfilename").trim() + "_"
				+ SpecialActions.convertTimestamptoText() + ".xlsx";
		Browser browser = new Browser();
		browser.setChromeDriver();
		ChromeOptions options = browser.setChromeOptions();
		driver = new ChromeDriver(options);
		browser.maximize(driver);
		String baseURL = context.getCurrentXmlTest().getParameter("url").trim();
		driver.get(baseURL);
		Login login = new Login(driver);
		String username = context.getCurrentXmlTest().getParameter("username").trim();
		String password = context.getCurrentXmlTest().getParameter("password").trim();
		login.enterUsername(username);
		login.enterPassword(password);
		login.submitCredentials();
		cognita = new Cognita(driver);
		wait = new CustomWait();

		/*
		 * Reset the drop downs to default because Akeley wood's initial data is incorrect
		 */
		try
		{
			cognita.selectRegion("ALL");
			cognita.selectCountry("ALL");
			cognita.selectSchool("ALL");
			cognita.submit();
			cognita.assertReset();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Assert.fail(Throwables.getStackTraceAsString(e));
			driver.quit();
		}
	}

	@Test(dataProvider = "cognitaglobal", dataProviderClass = CognitaDataProvider.class)
	public void runtest(String region, String country, String school, String campus)
	{
		try
		{
			if (SpecialActions.isValid(region) && SpecialActions.isValid(country) && SpecialActions.isValid(school)
					&& SpecialActions.isValid(campus))
			{
				cognita.selectRegion(region);
				cognita.selectCountry(country);
				cognita.selectSchool(school);
				cognita.selectCampus(campus);
				cognita.waitAndAssertSave();
				cognita.waitForLoadingWheeltoVanish();
				wait.waitForElementToBeDisplayed(driver, By.id("enq_chart_id"));
				cognita.waitForGraphtoLoad();

				/* Initiate excel creation of running test for first time */
				if (!doesExcelExist)
				{
					List<WebElement> years = driver.findElements(yearsAxis);
					String[] yearsarray = years.get(0).getText().split("\n");
					// Code for creating excel file
					write.createExcel(excelFilename, yearsarray);
					doesExcelExist = true;
				}

				/* Select YOE drop down & REPEAT THE BELOW CODE for all the YOE values */
				for (int yoeindex = 0; yoeindex < yoeData.length; yoeindex++)
				{
					cognita.selectYear(yoeData[yoeindex]);

					/* REPEAT THE BELOW CODE TWICE FOR MONTH & ACCUMULATED */
					for (int repeat = 0; repeat < 2; repeat++)
					{
						if (repeat == 0)
						{
							// Click In Month button
							wait.waitForElementToBeDisplayed(driver, inMonthbtn);
							driver.findElement(inMonthbtn).click();
						}
						else
						{
							// Click Accumulated
							wait.waitForElementToBeDisplayed(driver, accumulatedBtn);
							driver.findElement(accumulatedBtn).click();
						}

						/*
						 * This must be reset because the graph arrays of 228 rows; once for month and then once for accumulated
						 */
						ngraphrow = -1;
						List<List<String>> newgraphlist = new ArrayList<List<String>>();

						/* Wait for the graph to populate. It takes usually 1.03 seconds */
						Thread.sleep(2000);

						/* Now find months & store in final array */
						List<WebElement> fusionlist = null;
						By fusionlistselector = null;
						String months = null;
						if (repeat == 0) /* If graph is for month */
						{
							fusionlistselector = By.cssSelector("div#stacked_bar_8 span#enq_chart_id svg g:nth-child(5) g.fusioncharts-xaxis-0-gridlabels");
						}
						else /* if graph is for cumulative */
						{
							fusionlistselector = By.cssSelector("div#stacked_bar_8 span#enq_chart_id_cumulative svg g:nth-child(5) g.fusioncharts-xaxis-0-gridlabels");
						}

						wait.waitForElementToBeDisplayed(driver, fusionlistselector);
						fusionlist = driver.findElements(fusionlistselector);
						months = fusionlist.get(0).getText();
						String[] montharray = months.split("\n");
						String[][] finalarray = new String[montharray.length][11];

						/* Lets store the region, country, school & campus info */
						finalarray = getHeadingsArray(finalarray, region, country, school, campus);

						/* Now store the month data */
						for (int i = 0; i < finalarray.length; i++)
						{
							finalarray[i][4] = montharray[i];
						}

						/* FINDING THE STATS & STORING IN FINAL ARRAY */

						List<WebElement> elements = null;
						By graphType = null;
						String yaxis = null;
						if (repeat == 0) /* If graph is for month */
						{
							graphType = By.cssSelector("span#enq_chart_id svg g.fusioncharts-datalabels");
						}
						else /* if graph is for cumulative */
						{
							graphType = By.cssSelector("span#enq_chart_id_cumulative.fusioncharts-container svg g.fusioncharts-datalabels");
						}
						wait.waitForElementToBeDisplayed(driver, graphType);
						elements = driver.findElements(graphType);
						yaxis = elements.get(0).getText();
						String[] yaxisArray = yaxis.split("\n");

						/* Store the data obtained till now in a an array */
						int k = 0;
						finalarray = getFinalArrayfromYaxis(finalarray, yaxisArray, k, repeat, yoeData[yoeindex]);

						/* Click the graph to open the popup 12 times & store stats in a new graph */
						boolean alreadydone = false;
						String[] Xaxisarray = null;

						/* Click 12 times */
						for (int number = 1; number <= 12; number++)
						{
							By singlebar = null;
							if (repeat == 0)
							{
								singlebar = By.cssSelector("span#enq_chart_id.fusioncharts-container svg g:nth-child(10) g rect:nth-child("
										+ number + ")");
							}
							else
							{
								singlebar = By.cssSelector("span#enq_chart_id_cumulative.fusioncharts-container svg g:nth-child(10) g rect:nth-child("
										+ number + ")");
							}
							wait.waitForElementToBeDisplayed(driver, singlebar);
							driver.findElement(singlebar).click();
							Thread.sleep(3000);

							// If X axis data obtained already then dont repeat
							if (!alreadydone)
							{
								// Store X-axis of month graph = X Axis of accumulated graph also
								List<WebElement> Xaxispopup = driver.findElements(By.cssSelector("div#drilldown_chart_div span svg g:nth-child(5) g.fusioncharts-xaxis-0-gridlabels"));
								Xaxisarray = Xaxispopup.get(0).getText().split("\n");
								alreadydone = true;
							}

							/* Get the stats arraylist using Selenium */
							List<WebElement> graph;
							By graphselector = null;
							if (repeat == 0) // For month
							{
								graphselector = By.cssSelector("div#drilldown_chart_div.chart_container span svg g.fusioncharts-datalabels");
							}
							else // For InAccumulated
							{
								graphselector = By.cssSelector("div#drilldown_accumulated_chart_div span svg g.fusioncharts-datalabels");
							}

							String[] grapharray = null;

							/* Size of the graph array must be 76 */
							boolean correctsize = false;
							wait.waitForElementToBeDisplayed(driver, graphselector);
							while (!correctsize)
							{
								/* Get all stats in an array */
								try
								{
									graph = driver.findElements(graphselector);
									WebElement subchild = null;
									if (!graph.isEmpty() & graph != null)
									{
										subchild = graph.get(0);
										grapharray = subchild.getText().split("\n");
										if (grapharray != null && grapharray.length == Xaxisarray.length * 4)
										{
											correctsize = true;
										} 
									}
								}
								catch (WebDriverException | IndexOutOfBoundsException | NullPointerException e)
								{
									System.out.println(e.getClass().getSimpleName() + " was found for "
											+ yoeData[yoeindex]);
								}
							}

							/* Copy the stats array in a 19 x 4 arraylist */
							newgraphlist = getGraphList(newgraphlist, Xaxisarray, grapharray);

							/* Close the popup */
							((JavascriptExecutor) driver).executeScript("document.getElementById(\"hiddenClosePopupBtn\").click()");
							Thread.sleep(1000);
						}

						/* Create 19 x 4 graph array from arraylist */
						String[][] newgrapharray = convertListToArray(newgraphlist);

						/* Create new final array by adding the popup's x axis data of the graph */
						String[][] lastarray = new String[12 * Xaxisarray.length][16];
						lastarray = getLastArrfromFinalArr(finalarray, Xaxisarray, lastarray);

						/* Copy x axis in 12th column of last array */
						lastarray = copyXaxisLastArr(Xaxisarray, lastarray);

						/* COPY THE GRAPH ARRAY TO LAST ARRAY = 228 ROWS */
						lastarray = getUltimateArray(newgrapharray, lastarray);
						// System.out.println(Arrays.deepToString(lastarray));

						/* Finally write the sheet */
						write.writeSheet(lastarray);
					}
				}
			}
		}
		catch (Exception e)
		{
			System.out.println("Error in " + region + " " + country + " " + school + " " + campus);
			e.printStackTrace();
			Assert.fail(Throwables.getStackTraceAsString(e));
		}
	}

	private String[][] getUltimateArray(String[][] inputGraphArray, String[][] lastarray)
	{
		for (int i = 0; i < inputGraphArray.length; i++)
		{
			for (int j = 0, m = 12; j < inputGraphArray[i].length; j++, m++)
			{
				lastarray[i][m] = inputGraphArray[i][j];
			}
		}
		return lastarray;
	}

	private String[][] convertListToArray(List<List<String>> graphList)
	{
		String[][] temparray = new String[graphList.size()][];
		for (int i = 0; i < graphList.size(); i++)
		{
			List<String> dummy = graphList.get(i);
			temparray[i] = dummy.toArray(new String[dummy.size()]);
		}
		return temparray;
	}

	private List<List<String>> getGraphList(List<List<String>> graphList, String[] Xaxisarray, String[] grapharray)
	{
		int counter = 0;
		for (int i = 0; i < Xaxisarray.length; i++)
		{
			if (counter % 4 == 0) // If 4 cells have been populated then add new row
			{
				++ngraphrow;
				graphList.add(ngraphrow, new ArrayList<>());
			}
			for (counter = 0; counter < 4; counter++)
			{
				if (counter == 0)
				{
					graphList.get(ngraphrow).add(grapharray[i]);
				}
				else if (counter == 1)
				{
					graphList.get(ngraphrow).add(grapharray[i + Xaxisarray.length]);
				}
				else if (counter == 2)
				{
					graphList.get(ngraphrow).add(grapharray[i + (Xaxisarray.length * 2)]);
				}
				else if (counter == 3)
				{
					graphList.get(ngraphrow).add(grapharray[i + (Xaxisarray.length * 3)]);
					// System.out.println(Arrays.toString(newgraphlist.get(ngraphrow).toArray()));
				}
			}
		}
		return graphList;
	}

	private String[][] copyXaxisLastArr(String[] Xaxisarray, String[][] lastarray)
	{
		int counter = 0;
		int row = 0;
		while (counter < Xaxisarray.length)
		{
			for (int i = 0; i < Xaxisarray.length && row < 12 * Xaxisarray.length; i++, row++)
			{
				lastarray[row][11] = Xaxisarray[i];
			}
			++counter;
		}
		return lastarray;
	}

	private String[][] getLastArrfromFinalArr(String[][] finalarray, String[] Xaxisarray, String[][] lastarray)
	{
		int rowindex = 0;
		for (int row = 0; row < finalarray.length; row++)
		{
			int counter = 0;
			while (counter < Xaxisarray.length)
			{
				for (int column = 0; column < finalarray[row].length; column++)
				{
					lastarray[rowindex][column] = finalarray[row][column];
				}
				++rowindex;
				++counter;
			}
		}
		return lastarray;
	}

	private String[][] getFinalArrayfromYaxis(String[][] finalarray, String[] yaxisArray, int k, int repeatValue,
			String yoeValue)
	{
		for (int i = 0; i < finalarray.length; i++)
		{
			for (int j = 5; j < finalarray[i].length; j++)
			{
				if (j == 5)
				{
					finalarray[i][j] = yaxisArray[k];
				}
				else if (j == 6)
				{
					finalarray[i][j] = yaxisArray[k + 12];
				}
				else if (j == 7)
				{
					finalarray[i][j] = yaxisArray[k + 24];
				}
				else if (j == 8)
				{
					finalarray[i][j] = yaxisArray[k + 36];
				}
				else if (j == 9)
				{
					finalarray[i][j] = yoeValue;
				}
				else if (j == 10)
				{
					if (repeatValue == 0)
					{
						finalarray[i][j] = "Month";
					}
					else
					{
						finalarray[i][j] = "Accumulated";
					}
				}
			}
			++k;
		}
		return finalarray;
	}

	@AfterClass
	public void done()
	{
		try
		{
			write.writeExcelFile();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		if (driver != null)
		{
			driver.quit();
		}
	}

	private String[][] getHeadingsArray(String[][] array, String region, String country, String school, String campus)
	{
		for (int i = 0; i < array.length; i++)
		{
			for (int j = 0; j < 5; j++)
			{
				if (j == 0)
				{
					array[i][j] = region;
				}
				else if (j == 1)
				{
					array[i][j] = country;
				}
				else if (j == 2)
				{
					array[i][j] = school;
				}
				else if (j == 3)
				{
					array[i][j] = campus;
				}
			}
		}
		return array;
	}
}
