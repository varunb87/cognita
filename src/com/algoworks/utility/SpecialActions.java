package com.algoworks.utility;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class SpecialActions
{
	private static final int MAXCOLUMNS = 16;
	private static final String CHROMEPROPERTY = "webdriver.chrome.driver";
	private SpecialActions()
	{
		
	}

	public static int getMaxColumns()
	{
		return MAXCOLUMNS;
	}
	
	public static String getUserDirectory()
	{
		return System.getProperty("user.dir");
	}
	
	public static String getSeparator()
	{
		return System.getProperty("file.separator");
	}

	public static boolean isValid(String value)
	{
		return (StringUtils.isNotBlank(value) && StringUtils.isNotEmpty(value));
	}
	
	public static String getChromePropertyName()
	{
		return CHROMEPROPERTY;
	}

	public static String convertTimestamptoText()
	{
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		timeStamp = timeStamp.replace(".", "");
		return timeStamp;
	}
}
