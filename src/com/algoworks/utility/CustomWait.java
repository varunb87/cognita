package com.algoworks.utility;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CustomWait
{
	public void waitForDropdownToLoad(WebDriver driver, WebElement element)
	{
		FluentWait<WebDriver> wait = new FluentWait<>(driver);
		wait.pollingEvery(1, TimeUnit.SECONDS);
		wait.withTimeout(40, TimeUnit.SECONDS);
		wait.ignoring(StaleElementReferenceException.class, NoSuchElementException.class);

		wait.until(new Function<WebDriver, Boolean>()
		{
			public Boolean apply(WebDriver d)
			{
				final Select droplist = new Select(element);
				return (droplist.getOptions().size() > 1);
			}
		});
	}

	public void waitForElement(WebDriver driver, By locator) throws InterruptedException
	{
		WebDriverWait wait = new WebDriverWait(driver, 15);
		Boolean isVisible = false;
		while (!isVisible)
		{
			Thread.sleep(100);
			try
			{
				wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
				isVisible = true;
			}
			catch (StaleElementReferenceException e)
			{
			}
		}
	}
	
	public void waitForElementToBeDisplayed(WebDriver driver, By locator) throws InterruptedException
	{
		Boolean isVisible = false;
		int attempts = 0;
		while (!isVisible && attempts < 30)
		{
			Thread.sleep(500);
			try
			{
				driver.findElement(locator).isDisplayed();
				isVisible = true;
			}
			catch (WebDriverException e)
			{
				++attempts;
			}
		}
	}

	public void waitForElement(WebDriver driver, WebElement element)
	{
		WebDriverWait wait = new WebDriverWait(driver, 5);
		Boolean isVisible = false;
		int attempts = 0;
		while (!isVisible && attempts < 10)
		{
			try
			{
				wait.until(ExpectedConditions.visibilityOf(element));
				isVisible = true;
			}
			catch (StaleElementReferenceException e)
			{
				++attempts;
			} 
		}
	}

	public boolean waitForSaveMsg(WebDriver driver, WebElement element) throws InterruptedException
	{
		WebDriverWait wait = new WebDriverWait(driver, 5);
		Boolean isVisible = false;
		while (!isVisible)
		{
			Thread.sleep(100);
			try
			{
				wait.until(ExpectedConditions.visibilityOf(element));
				isVisible = true;
			}
			catch (StaleElementReferenceException e)
			{
			}
		}
		return isVisible;
	}
}
