package com.algoworks.dataprovider;

import java.io.IOException;
import java.util.Arrays;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import com.algoworks.utility.ReadExcel;
import com.algoworks.utility.SpecialActions;

public class CognitaDataProvider
{
	private CognitaDataProvider()
	{
	}

	@DataProvider(name = "cognitaglobal")
	public static String[][] getData(final ITestContext context) throws InvalidFormatException, IOException
	{
		String[][] testData = null;
		ReadExcel read = new ReadExcel();
		String filename = context.getCurrentXmlTest().getParameter("filtercriteria").trim();
		testData = (String[][]) read.returnArray(SpecialActions.getUserDirectory() + SpecialActions.getSeparator() + "src" + SpecialActions.getSeparator() + filename);
		String[][] dp = new String[testData.length - 1][];
		for (int row = 0; row < testData.length - 1; row++)
		{
			dp[row] = Arrays.copyOfRange(testData[row + 1], 0, testData[row + 1].length);
		}
		return dp;
	}

}
