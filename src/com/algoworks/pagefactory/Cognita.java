package com.algoworks.pagefactory;

import static org.testng.Assert.assertEquals;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.algoworks.utility.CustomWait;

public class Cognita
{
	private WebDriver driver;

	@FindBy(xpath = "//select[@name='region']")
	WebElement region;

	@FindBy(xpath = "//select[@name='country']")
	WebElement country;

	@FindBy(xpath = "//select[@name='school']")
	WebElement school;

	@FindBy(xpath = "//select[@name='campus']")
	WebElement campus;

	@FindBy(name = "save")
	WebElement save;

	@FindBy(id = "enq_chart_id_status")
	WebElement completion;

	@FindBy(className = "form_result_message")
	WebElement parameterSuccess;

	@FindBy(id = "custom_result_error_message")
	WebElement failureMsg;

	@FindBy(xpath = "//div[@class='form_result_top']/div[2]")
	WebElement closeButton;

	@FindBy(className = "loading-icon-visible")
	WebElement loadingwheel;

	@FindBy(id = "yoe_year_select")
	WebElement yearmenu;

	private CustomWait wait;
	private static final String SUCCESS = "Parameter values saved successfully";
	private static final String FAILURE = "Please choose schools which follow the same academic calendar";
	private WebDriverWait driverWait;

	public Cognita(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
		wait = new CustomWait();
		driverWait = new WebDriverWait(driver, 30);
	}

	public void selectRegion(String regionValue)
	{
		// wait.waitForDropdownToLoad(driver, region);
		int attempts = 0;
		boolean found = false;
		Select selectRegion;
		while (!found && attempts <= 40)
		{
			try
			{
				region.isDisplayed();
				selectRegion = new Select(region);
				if (!selectRegion.getFirstSelectedOption().getText().equalsIgnoreCase(regionValue))
				{
					selectRegion.selectByVisibleText(regionValue);
				}
				found = true;
			}
			catch (WebDriverException e)
			{
				++attempts;
			}
		}
	}

	public void selectCountry(String countryValue)
	{
		wait.waitForDropdownToLoad(driver, country);
		Select selectCountry = new Select(country);
		if (!selectCountry.getFirstSelectedOption().getText().equalsIgnoreCase(countryValue))
		{
			selectCountry.selectByVisibleText(countryValue);
		}
	}

	public void selectSchool(String schoolValue)
	{
		Select selectSchool = new Select(school);
		try
		{
			if (!selectSchool.getFirstSelectedOption().getText().equalsIgnoreCase(schoolValue))
			{
				selectSchoolwithValue(schoolValue, selectSchool);
			}
		}
		catch (NoSuchElementException e)
		{
			selectSchoolwithValue(schoolValue, selectSchool);
		}
	}

	/**
	 * @param schoolValue
	 * @param dropdown
	 */
	private void selectSchoolwithValue(String schoolValue, Select dropdown)
	{
		dropdown.deselectAll();
		dropdown.selectByVisibleText(schoolValue);
	}

	public void selectCampus(String campusValue)
	{
		wait.waitForDropdownToLoad(driver, campus);
		Select selectCampus = new Select(campus);
		selectCampus.selectByVisibleText(campusValue);
	}

	public void selectYear(String year)
	{
		int attempts = 0;
		boolean found = false;
		Select selectYear;

		while (!found && attempts <= 40)
		{
			try
			{
				yearmenu.isDisplayed();
				selectYear = new Select(yearmenu);
				if (selectYear.getOptions().size() > 1)
				{
					selectYear.selectByVisibleText(year);
					found = true;
				}
			}
			catch (StaleElementReferenceException e)
			{
				++attempts;
			} 
		}
	}

	public void waitForGraphtoLoad() throws InterruptedException
	{
		boolean isVisible = false;
		while (!isVisible)
		{
			Thread.sleep(1000); /* This must always be >= 1 second */
			try
			{
				driverWait.until(ExpectedConditions.attributeContains(completion, "value", "Completed"));
				isVisible = true;
			}
			catch (StaleElementReferenceException e)
			{
			}
		}
	}

	public void submit()
	{
		driverWait.until(ExpectedConditions.visibilityOf(save));
		save.click();
	}

	public WebElement getSuccessAlertElement()
	{
		return parameterSuccess;
	}

	public void assertSaved() throws InterruptedException
	{
		wait.waitForElement(driver, parameterSuccess);
		while (StringUtils.isBlank(parameterSuccess.getText()) && StringUtils.isEmpty(parameterSuccess.getText()))
		{
			if (StringUtils.isNotBlank(parameterSuccess.getText()) && StringUtils.isNotEmpty(parameterSuccess.getText()))
			{
				break;
			}
		}
		assertEquals(parameterSuccess.getText(), SUCCESS);
	}

	public void waitAndAssertSave() throws InterruptedException
	{
		boolean isVisible = false;
		while (!isVisible)
		{
			try
			{
				submit();
				assertSaved();
				isVisible = true;
			}
			catch (TimeoutException | StaleElementReferenceException | AssertionError e)
			{
			}
		}
	}

	public void assertReset() throws InterruptedException
	{
		wait.waitForElement(driver, failureMsg);
		assertEquals(failureMsg.getText(), FAILURE);
	}

	public void waitForLoadingWheeltoVanish()
	{
		driverWait.until(ExpectedConditions.invisibilityOf(loadingwheel));
	}
}
