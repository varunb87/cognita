package com.algoworks.utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tools.ant.DirectoryScanner;

public class WriteExcelPipeline
{
	private Workbook book;
	private Sheet sheet;
	private Row row;
	private String filename = null;
	private File file = null;
	private static Logger logger = Logger.getLogger(WriteExcelPipeline.class.getName());
	private int rowindex = 1;

	public void deleteExistingFile()
	{
		DirectoryScanner scanner = new DirectoryScanner();
		scanner.setIncludes(new String[]{"*.xlsx"});
		scanner.setBasedir(System.getProperty("user.dir"));
		scanner.setCaseSensitive(false);
		scanner.scan();
		String[] files = scanner.getIncludedFiles();
		for(String f: files) {
			System.out.println("Deleting file ----- " + f);
			file = new File(f);
			if (file.exists())
			{
				Boolean deleted = file.delete();
				if (deleted)
				{
					logger.info("Existing file erased. New file will be generated.");
				}
			}
		}
	}

	public void createExcel(String filename, String sheetname, String[][] headingsarray, String sheetType) /* sheetType
																											 * = Week,
																											 * Month,
																											 * Academic
																											 * Year */
	{
		deleteExistingFile();
		this.filename = System.getProperty("user.dir") + SpecialActions.getSeparator() + filename;
		file = new File(this.filename);
		if (book == null)
		{
			book = new XSSFWorkbook();
		}
		String newsheetname = WorkbookUtil.createSafeSheetName(sheetname);
		sheet = book.createSheet(newsheetname);
		row = sheet.createRow(0);
		int totalsize = headingsarray[0].length + 5;
		for (int column = 0; column < totalsize; column++)
		{
			Cell cell = row.createCell(column, CellType.STRING);
			if (column == 0)
			{
				cell.setCellValue(headingsarray[0][0]);
			}
			else if (column == 1)
			{
				cell.setCellValue(headingsarray[0][1]);
			}
			else if (column == 2)
			{
				cell.setCellValue(headingsarray[0][2]);
			}
			else if (column == 3)
			{
				cell.setCellValue(headingsarray[0][3]);
			}
			else if (column == 4)
			{
				cell.setCellValue(headingsarray[0][4]);
			}
			else if (column == 5)
			{
				cell.setCellValue(headingsarray[0][5]);
			}
			else if (column == 6)
			{
				cell.setCellValue(headingsarray[0][6]);
			}
			else if (column == 7)
			{
				cell.setCellValue(headingsarray[0][7]);
			}
			else if (column == 8)
			{
				cell.setCellValue(headingsarray[0][8]);
			}
			else if (column == 9)
			{
				cell.setCellValue(headingsarray[0][9]);
			}
			else if (column == 10)
			{
				cell.setCellValue(headingsarray[0][10]);
			}
			else if (column == 11)
			{
				cell.setCellValue(headingsarray[0][11]);
			}
			else if (column == 12)
			{
				cell.setCellValue(headingsarray[0][12]);
			}
		}
	}
	
	public void resetRowIndex()
	{
		rowindex = 1;
	}

	public void writeSheet(String[][] data)
	{
		int maxrows = data.length;
		for (int i = 0; i < maxrows; i++)
		{
			row = sheet.createRow(rowindex);
			int maxColumns = data[i].length;
			for (int columnIndex = 0; columnIndex < maxColumns; columnIndex++)
			{
				Cell cell = row.createCell(columnIndex, CellType.STRING);
				cell.setCellValue(data[i][columnIndex]);
			}
			++rowindex;
		}
	}

	public void writeExceltoDrive() throws IOException
	{
		FileOutputStream output = new FileOutputStream(filename, true);
		book.write(output);
		book.close();
		output.close();
	}
	
	/*private boolean sheetExists(String sheetType)
	{
		if (sheetType.equalsIgnoreCase(this.sheetType))
		{
			this.sheetType = sheetType;
			return true;
		}
		else
			return false;
	}*/
}