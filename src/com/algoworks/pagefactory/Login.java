package com.algoworks.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.algoworks.utility.CustomWait;

public class Login
{
	@FindBy(id="_58_login")
	WebElement loginbox;
	
	@FindBy(id="_58_password")
	WebElement passwordbox;
	
	@FindBy(css = "input.aui-button-input.aui-button-input-submit")
	WebElement submit;
	
	WebDriver driver;
	private CustomWait wait;
	
	public Login(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
		wait = new CustomWait();
	}
	
	public void enterUsername(String username)
	{
		wait.waitForElement(driver, loginbox);
		loginbox.sendKeys(username);
	}
	
	public void enterPassword(String password)
	{
		wait.waitForElement(driver, passwordbox);
		passwordbox.sendKeys(password);
	}
	
	public void submitCredentials()
	{
		wait.waitForElement(driver, submit);
		submit.click();
	}
}
