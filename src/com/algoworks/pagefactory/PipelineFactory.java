package com.algoworks.pagefactory;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.algoworks.utility.CustomWait;

public class PipelineFactory
{

	@FindBy(xpath = "//ul[@id='pipeline']/li[1]")
	WebElement weekmenu;
	
	@FindBy(xpath = "//ul[@id='pipeline']/li[2]")
	WebElement monthmenu;
	
	@FindBy(xpath = "//ul[@id='pipeline']/li[3]")
	WebElement yearmenu;
	
	@FindBy(xpath = "//div[@class='admissions-analytic-container']/div[2]/h2")
	WebElement report;
	
	private WebDriver driver;
	private CustomWait wait = null;
	
	public PipelineFactory(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
		wait = new CustomWait();
	}
	
	public void clickWeek() throws InterruptedException
	{
		boolean isVisible = false;
		while (!isVisible)
		{
			try
			{
				wait.waitForElement(driver, weekmenu);
				weekmenu.click();
				isVisible = true;
			}
			catch (TimeoutException e)
			{
				driver.get(driver.getCurrentUrl());
			}
		}
	}
	
	public void clickmonth() throws InterruptedException
	{
		boolean isVisible = false;
		while (!isVisible)
		{
			try
			{
				wait.waitForElement(driver, monthmenu);
				monthmenu.click();
				isVisible = true;
			}
			catch (TimeoutException e)
			{
				driver.get(driver.getCurrentUrl());
			}
		}
	}
	
	public void clickyear() throws InterruptedException
	{
		boolean isVisible = false;
		while (!isVisible)
		{
			try
			{
				wait.waitForElement(driver, yearmenu);
				yearmenu.click();
				isVisible = true;
			}
			catch (TimeoutException e)
			{
				driver.get(driver.getCurrentUrl());
			}
		}
	}
	
	public void waitForReportVisibility() throws InterruptedException
	{
		wait.waitForElement(driver, report);
		while (StringUtils.isEmpty(report.getText()) && StringUtils.isBlank(report.getText()))
		{
			if (StringUtils.isNotBlank(report.getText()) && StringUtils.isNotEmpty(report.getText()))
			{
				break;
			}
		}
	}
	
	public String getReportName()
	{
		return report.getText();
	}
}