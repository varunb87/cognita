package com.algoworks.cognita;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.algoworks.dataprovider.CognitaDataProvider;
import com.algoworks.pagefactory.Cognita;
import com.algoworks.pagefactory.Login;
import com.algoworks.pagefactory.PipelineFactory;
import com.algoworks.utility.Browser;
import com.algoworks.utility.CustomWait;
import com.algoworks.utility.SpecialActions;
import com.algoworks.utility.WriteExcelPipeline;
import com.google.common.base.Throwables;

public class Pipeline
{
	private WebDriver driver;
	private WriteExcelPipeline write = null;
	private Cognita cognita;
	private String excelFilename;
	private PipelineFactory pipefactory = null;
	private String timeperiod = null;
	private CustomWait wait;
	private String reportname = "";

	@BeforeClass
	public void setup(final ITestContext context)
	{
		excelFilename = context.getCurrentXmlTest().getParameter("outputfilename").trim() + "_" + SpecialActions.convertTimestamptoText() + ".xlsx";
		write = new WriteExcelPipeline();
		Browser browser = new Browser();
		browser.setChromeDriver();
		ChromeOptions options = browser.setChromeOptions();
		driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		browser.maximize(driver);
		String baseURL = context.getCurrentXmlTest().getParameter("url").trim();
		driver.get(baseURL);
		Login login = new Login(driver);
		String username = context.getCurrentXmlTest().getParameter("username").trim();
		String password = context.getCurrentXmlTest().getParameter("password").trim();
		login.enterUsername(username);
		login.enterPassword(password);
		login.submitCredentials();
		cognita = new Cognita(driver);
		wait = new CustomWait();
		driver.findElement(By.xpath("//nav[@id='navigation']/ul/li[2]/a/span")).click();
		Alert alert = driver.switchTo().alert();
		alert.accept();
		pipefactory = new PipelineFactory(driver);
	}

	@Test(dataProvider = "cognitaglobal", dataProviderClass = CognitaDataProvider.class)
	public void runpipeline(String region, String country, String school, String campus, String timeperiod)
	{
		try
		{
			cognita.selectRegion(region);
			cognita.selectCountry(country);
			cognita.selectSchool(school);
			cognita.selectCampus(campus);
			cognita.waitAndAssertSave();
			cognita.waitForLoadingWheeltoVanish();
			if ("week".equalsIgnoreCase(timeperiod))
			{
				pipefactory.clickWeek();
			}
			else if ("month".equalsIgnoreCase(timeperiod))
			{
				pipefactory.clickmonth();
			}
			else if ("Year".contains(timeperiod))
			{
				pipefactory.clickyear();
			}
			cognita.waitForLoadingWheeltoVanish();

			/* Wait for report name to appear */
			pipefactory.waitForReportVisibility();
			
			driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);

			/* Get the table headings in array list */
			wait.waitForElement(driver, By.xpath("//table[@id='am_pipeline_table']/thead/tr[1]/th[1]"));
			List<List<String>> headings = extractList("thead", "th");

			/* Get the table data in arraylist */
			wait.waitForElement(driver, By.xpath("//table[@id='am_pipeline_table']/tbody/tr[1]/td[1]"));
			List<List<String>> data = extractList("tbody", "td");

			/* Clone the headings list to another list. This is important to add more data BEFORE the original data */
			List<List<String>> clone = getClonedArrayList(headings);
			headings = getFinalList(headings, clone, "Region", "Country", "School", "Campus", "Time Frame");

			/* Create excel file with separate sheet. IMPORTANT! This needs to be done only once for each timeframe */
			if (!timeperiod.equalsIgnoreCase(this.timeperiod))
			{
				String[][] headingsarray = getArrayofList(headings);
				
				/* Sometimes the report name is the same for different time frame. This will need clicking the save button again */
				System.out.println();
				System.out.println("Previous report name = " + reportname);
				reportname = verifyReportName(reportname);
				System.out.println("New report name = " + reportname);
				
				write.createExcel(excelFilename, reportname, headingsarray, timeperiod);
				this.timeperiod = timeperiod;
				write.resetRowIndex();
			}

			/* Now create clone of data */
			clone = getClonedArrayList(data);
			data = getFinalList(data, clone, region, country, school, campus, timeperiod);

			/* Send data to sheets */
			String[][] dataarray = getArrayofList(data);

			/* Display data on console */
			displayOnConsole(dataarray);

			/* Write data in sheet. If timeperiod changes then new sheet in same file will be created */
			write.writeSheet(dataarray);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Assert.fail(Throwables.getStackTraceAsString(e));
		}
		System.out.println();
		System.out.println();
	}

	@AfterClass
	public void done()
	{
		try
		{
			write.writeExceltoDrive();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			Assert.fail(Throwables.getStackTraceAsString(e));
		}
		if (driver != null)
		{
			driver.quit();
		}
	}
	
	private String verifyReportName(String reportname) throws InterruptedException
	{
		boolean isValid = false;
		while(!isValid)
		{
			String currentReportname = pipefactory.getReportName();
			if(reportname.equalsIgnoreCase(currentReportname))
			{
				cognita.waitAndAssertSave();
				pipefactory.waitForReportVisibility();
			}
			else
			{
				isValid = true;
				reportname = currentReportname;
			}
		}
		return reportname;
	}

	private void displayOnConsole(String[][] dataarray)
	{
		for (int i = 0; i < dataarray.length; i++)
		{
			for (int j = 0; j < dataarray[i].length; j++)
			{
				System.out.print(dataarray[i][j] + "\t \t");
			}
			System.out.println();
		}
	}

	private List<List<String>> getClonedArrayList(List<List<String>> originalList)
	{
		List<List<String>> clone = new ArrayList<>();
		for (List<String> temp : originalList)
		{
			clone.add(temp);
		}
		return clone;
	}

	private String[][] getArrayofList(List<List<String>> tempList)
	{
		String[][] array = new String[tempList.size()][];
		for (int i = 0; i < tempList.size(); i++)
		{
			List<String> dummy = tempList.get(i);
			array[i] = dummy.toArray(new String[dummy.size()]);
		}
		return array;
	}

	private List<List<String>> getFinalList(List<List<String>> mainlist, List<List<String>> clone, String value1, String value2, String value3, String value4, String value5)
	{
		mainlist.clear();
		for (int index = 0; index < clone.size(); index++)
		{
			mainlist.add(index, new ArrayList<>());
			for (int subindex = 0; subindex < clone.get(index).size() + 5; subindex++)
			{
				if (subindex == 0)
				{
					mainlist.get(index).add(subindex, value1);
				}
				else if (subindex == 1)
				{
					mainlist.get(index).add(subindex, value2);
				}
				else if (subindex == 2)
				{
					mainlist.get(index).add(subindex, value3);
				}
				else if (subindex == 3)
				{
					mainlist.get(index).add(subindex, value4);
				}
				else if (subindex == 4)
				{
					mainlist.get(index).add(subindex, value5);
				}
				else
				{
					mainlist.get(index).add(subindex, clone.get(index).get(subindex - 5));
				}
			}
		}
		return mainlist;
	}

	private List<List<String>> extractList(String value, String tag)
	{
		List<List<String>> templist = new ArrayList<>();
		boolean iexists = true;
		boolean jexists = true;
		for (int i = 1; iexists; i++)
		{
			try
			{
				driver.findElement(By.xpath("//table[@id='am_pipeline_table']/" + value + "/tr[" + i + "]"));
				templist.add(new ArrayList<>());
				jexists = true;
				for (int j = 1; jexists; j++)
				{
					try
					{
						String temp = driver.findElement(By.xpath("//table[@id='am_pipeline_table']/" + value + "/tr[" + i + "]/" + tag + "[" + j + "]")).getText();
						if (StringUtils.isNotBlank(temp) && StringUtils.isNotEmpty(temp))
						{
							templist.get(i - 1).add(temp);
						}
					}
					catch (WebDriverException e)
					{
						jexists = false;
					}
				}
			}
			catch (WebDriverException e)
			{
				iexists = false;
			}
		}
		return templist;
	}
}
