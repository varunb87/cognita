package com.algoworks.utility;

import java.io.File;

import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Browser
{
	private static final String BINARIES_FOLDER = "Executables";
	public ChromeOptions setChromeOptions()
	{
		ChromeOptions options = new ChromeOptions();
		if (SystemUtils.IS_OS_WINDOWS)
		{
			options.addArguments("--start-maximized");
		}
		options.addArguments("--disable-web-security");
		options.addArguments("--ignore-certificate-errors");
		return options;
	}
	
	public void setChromeDriver()
	{
		if (SystemUtils.IS_OS_WINDOWS)
		{
			System.setProperty(SpecialActions.getChromePropertyName(), SpecialActions.getUserDirectory() + File.separator + BINARIES_FOLDER + File.separator + "chromedriver.exe");
		}
		else if (SystemUtils.IS_OS_LINUX)
		{
			System.setProperty(SpecialActions.getChromePropertyName(), SpecialActions.getUserDirectory() + File.separator + BINARIES_FOLDER + File.separator + "chromedriver");
		}
		else if (SystemUtils.IS_OS_MAC_OSX)
		{
			System.setProperty(SpecialActions.getChromePropertyName(), SpecialActions.getUserDirectory() + File.separator + BINARIES_FOLDER + File.separator + "chromedriver_mac");
		}
	}
	
	public void maximize(WebDriver driver)
	{
		if (SystemUtils.IS_OS_MAC_OSX)
		{
			driver.manage().window().fullscreen();
		}
	}
}