package com.algoworks.utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tools.ant.DirectoryScanner;

public class WriteExcel
{
	private Workbook book;
	private Sheet sheet;
	private Row row;
	private String filename = null;
	private File file = null;
	private static Logger logger = Logger.getLogger(WriteExcel.class.getName());
	private int rowindex = 1;

	public void deleteExistingFile()
	{
		DirectoryScanner scanner = new DirectoryScanner();
		scanner.setIncludes(new String[]{"*.xlsx"});
		scanner.setBasedir(SpecialActions.getUserDirectory());
		scanner.setCaseSensitive(false);
		scanner.scan();
		String[] files = scanner.getIncludedFiles();
		for(String f: files) {
			System.out.println("Deleting file ----- " + f);
			file = new File(f);
			if (file.exists())
			{
				Boolean deleted = file.delete();
				if (deleted)
				{
					logger.info(f + " erased. New file will be generated.");
				}
			}
		}
	}

	public void createExcel(String filename, String[] yearsarray)
	{
		deleteExistingFile();
		this.filename = SpecialActions.getUserDirectory() + SpecialActions.getSeparator() + filename;
		file = new File(this.filename);
		if (book == null)
		{
			book = new XSSFWorkbook();
		}
		sheet = book.createSheet();
		row = sheet.createRow(0);
		for (int columnIndex = 0; columnIndex < SpecialActions.getMaxColumns(); columnIndex++)
		{
			Cell cell = row.createCell(columnIndex, CellType.STRING);
			if (columnIndex == 0)
			{
				cell.setCellValue("Region");
			}
			else if (columnIndex == 1)
			{
				cell.setCellValue("Country");
			}
			else if (columnIndex == 2)
			{
				cell.setCellValue("School");
			}
			else if (columnIndex == 3)
			{
				cell.setCellValue("Campus");
			}
			else if (columnIndex == 4)
			{
				cell.setCellValue("Month");
			}
			else if (columnIndex == 5)
			{
				cell.setCellValue(yearsarray[0]);
			}
			else if (columnIndex == 6)
			{
				cell.setCellValue(yearsarray[1]);
			}
			else if (columnIndex == 7)
			{
				cell.setCellValue(yearsarray[2]);
			}
			else if (columnIndex == 8)
			{
				cell.setCellValue(yearsarray[3]);
			}
			else if (columnIndex == 9)
			{
				cell.setCellValue("YOE");
			}
			else if (columnIndex == 10)
			{
				cell.setCellValue("Month/Accumulated");
			}
			else if (columnIndex == 11)
			{
				cell.setCellValue("Grade");
			}
			else if (columnIndex == 12)
			{
				cell.setCellValue(yearsarray[0]);
			}
			else if (columnIndex == 13)
			{
				cell.setCellValue(yearsarray[1]);
			}
			else if (columnIndex == 14)
			{
				cell.setCellValue(yearsarray[2]);
			}
			else if (columnIndex == 15)
			{
				cell.setCellValue(yearsarray[3]);
			}
		}
	}

	public void writeSheet(String[][] data)
	{
		int maxrows = data.length;
		for (int i = 0; i < maxrows; i++)
		{
			row = sheet.createRow(rowindex);
			int maxColumns = data[i].length;
			for (int columnIndex = 0; columnIndex < maxColumns; columnIndex++)
			{
				Cell cell = row.createCell(columnIndex, CellType.STRING);
				cell.setCellValue(data[i][columnIndex]);
			}
			++rowindex;
		}
	}

	public void writeExcelFile() throws IOException
	{
		FileOutputStream output = new FileOutputStream(filename, true);
		book.write(output);
		book.close();
		output.close();
	}
}
